all: usb-reset

usb-reset: usb-reset.c
	gcc -O3 -o usb-reset usb-reset.c

clean:
	rm -rf usb-reset
